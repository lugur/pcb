# -*- coding: utf-8 -*-

##########################################################################
# OpenLP - Open Source Lyrics Projection                                 #
# ---------------------------------------------------------------------- #
# Copyright (c) 2008-2023 OpenLP Developers                              #
# ---------------------------------------------------------------------- #
# This program is free software: you can redistribute it and/or modify   #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public License      #
# along with this program.  If not, see <https://www.gnu.org/licenses/>. #
##########################################################################
"""
The :mod:`~openlp.plugins.pcb.forms.selectplandialog` module contains the user interface code for the dialog
"""
from PyQt5 import QtWidgets

from openlp.core.common.i18n import translate


class Ui_SelectPlanDialog(object):
    """
    The actual Qt components that make up the dialog.
    """
    def setup_ui(self, pcb_dialog):
        pcb_dialog.setObjectName('pcb_dialog')
        pcb_dialog.resize(400, 280)
        self.pcb_layout = QtWidgets.QFormLayout(pcb_dialog)
        self.pcb_layout.setContentsMargins(50, 50, 50, 50)
        self.pcb_layout.setSpacing(8)
        self.pcb_layout.setFieldGrowthPolicy(QtWidgets.QFormLayout.ExpandingFieldsGrow)
        # Service Type GUI Elements -- service_type combo_box
        self.service_type_label = QtWidgets.QLabel(pcb_dialog)
        self.service_type_combo_box = QtWidgets.QComboBox(pcb_dialog)
        self.pcb_layout.addRow(self.service_type_label, self.service_type_combo_box)
        # Plan Selection GUI Elements
        self.plan_selection_label = QtWidgets.QLabel(pcb_dialog)
        self.plan_selection_combo_box = QtWidgets.QComboBox(pcb_dialog)
        self.pcb_layout.addRow(self.plan_selection_label, self.plan_selection_combo_box)
        # Theme List for Songs and Custom Slides
        self.song_theme_selection_label = QtWidgets.QLabel(pcb_dialog)
        self.song_theme_selection_combo_box = QtWidgets.QComboBox(pcb_dialog)
        self.pcb_layout.addRow(self.song_theme_selection_label, self.song_theme_selection_combo_box)
        self.slide_theme_selection_label = QtWidgets.QLabel(pcb_dialog)
        self.slide_theme_selection_combo_box = QtWidgets.QComboBox(pcb_dialog)
        self.pcb_layout.addRow(self.slide_theme_selection_label, self.slide_theme_selection_combo_box)
        # Import Button
        self.button_layout = QtWidgets.QDialogButtonBox(pcb_dialog)
        self.import_as_new_button = QtWidgets.QPushButton(pcb_dialog)
        self.button_layout.addButton(self.import_as_new_button, QtWidgets.QDialogButtonBox.AcceptRole)
        self.update_existing_button = QtWidgets.QPushButton(pcb_dialog)
        self.button_layout.addButton(self.update_existing_button, QtWidgets.QDialogButtonBox.AcceptRole)
        self.edit_auth_button = QtWidgets.QPushButton(pcb_dialog)
        self.button_layout.addButton(self.edit_auth_button, QtWidgets.QDialogButtonBox.ActionRole)
        self.pcb_layout.addRow(self.button_layout)
        self.retranslate_ui(pcb_dialog)

    def retranslate_ui(self, pcb_dialog):
        """
        Translate the GUI.
        """
        pcb_dialog.setWindowTitle(translate('PcbPlugin.PcbForm',
                                                       'Planning Center Online Service Importer'))
        self.service_type_label.setText(translate('PcbPlugin.PcbForm', 'Service Type'))
        self.plan_selection_label.setText(translate('PcbPlugin.PcbForm', 'Select Plan'))
        self.import_as_new_button.setText(translate('PcbPlugin.PcbForm', 'Import New'))
        self.import_as_new_button.setToolTip(translate('PcbPlugin.PcbForm',
                                                       'Import As New Service'))
        self.update_existing_button.setText(translate('PcbPlugin.PcbForm', 'Refresh Service'))
        self.update_existing_button.setToolTip(translate('PcbPlugin.PcbForm',
                                                         'Refresh Existing Service from Planning Center. '
                                                         'This will update song lyrics or item orders that '
                                                         'have changed'))
        self.edit_auth_button.setText(translate('PcbPlugin.PcbForm', 'Edit Authentication'))
        self.edit_auth_button.setToolTip(translate('PcbPlugin.PcbForm', 'Edit the Application '
                                                   'ID and Secret Code to login to Planning Center Online'))
        self.song_theme_selection_label.setText(translate('PcbPlugin.PcbForm', 'Song Theme'))
        self.slide_theme_selection_label.setText(translate('PcbPlugin.PcbForm', 'Slide Theme'))
