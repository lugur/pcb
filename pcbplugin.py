# -*- coding: utf-8 -*-

##########################################################################
# OpenLP - Open Source Lyrics Projection                                 #
# ---------------------------------------------------------------------- #
# Copyright (c) 2008-2023 OpenLP Developers                              #
# ---------------------------------------------------------------------- #
# This program is free software: you can redistribute it and/or modify   #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public License      #
# along with this program.  If not, see <https://www.gnu.org/licenses/>. #
##########################################################################
"""
The :mod:`~openlp.plugins.pcb.PcbPlugin` module contains
the Plugin class for the Pcb plugin.
"""
import logging

log = logging.getLogger(__name__)


from openlp.core.common.i18n import translate
from openlp.core.common.registry import Registry
from openlp.core.lib.plugin import Plugin, StringContent
from openlp.core.lib.ui import create_action
from openlp.core.state import State
from openlp.core.ui.icons import UiIcons
from .forms.selectplanform import SelectPlanForm
from .lib.pcbtab import PcbTab

import sys
try:
    log.info('trying to load pcb plugin loaded')
    __import__('openlp.plugins.pcb')
except ImportError:
    log.info('trying to load pcb plugin loaded')
    __import__('plugins.pcb') 
    sys.modules['openlp.plugins.pcb'] = sys.modules.pop('plugins.pcb')


class PcbPlugin(Plugin):
    """
    This plugin enables the user to import services from Planning Center Online.
    """
    log.info('pcb Plugin loaded')

    def __init__(self):
        """
        Create and set up the Pcb plugin.
        """
        super(PcbPlugin, self).__init__('pcb', settings_tab_class=PcbTab)
        self.pcb_form = None
        self.icon = UiIcons().planning_center
        self.icon_path = self.icon
        self.weight = -1
        self.settings.extend_default_settings({
            'pcb/status': False,
            'pcb/application_id': '',
            'pcb/secret': ''
        })
        State().add_service('pcb', self.weight, is_plugin=True)
        State().update_pre_conditions('pcb', self.check_pre_conditions())


    def initialise(self):
        """
        Initialise the plugin
        """
        log.info('Pcb Initialising')
        super(PcbPlugin, self).initialise()
        self.import_planning_center.setVisible(True)

    def add_import_menu_item(self, import_menu):
        """
        Add "Pcb Service" to the **Import** menu.

        :param import_menu: The actual **Import** menu item, so that your
        actions can use it as their parent.
        """
        self.import_planning_center = create_action(import_menu, 'import_planning_center',
                                                    text=translate('PcbPlugin', 'Planning Center Service'),
                                                    visible=False,
                                                    statustip=translate('PcbPlugin',
                                                                        'Import Planning Center Service Plan '
                                                                        'from Planning Center Online.'),
                                                    triggers=self.on_import_planning_center_triggered
                                                    )
        import_menu.addAction(self.import_planning_center)

    def on_import_planning_center_triggered(self):
        """
        Run the Pcb importer.
        """
        # Determine which dialog to show based on whether the auth values are set yet
        self.application_id = self.settings.value("pcb/application_id")
        self.secret = self.settings.value("pcb/secret")
        if len(self.application_id) == 0 or len(self.secret) == 0:
            self.pcb_form = Registry().get('settings_form')
            self.pcb_form.exec(translate('PcbPlugin', 'Pcb'))
        else:
            self.pcb_form = SelectPlanForm(Registry().get('main_window'), self)
            self.pcb_form.exec()

    @staticmethod
    def about():
        """
        Provides information for the plugin manager to display.

        :return: A translatable string with some basic information about the
        Pcb plugin
        """
        return translate('PcbPlugin', '<strong>Pcb Plugin</strong>'
                         '<br />The pcb plugin provides an interface to import '
                         'service plans from the Planning Center Online v2 API. Rewritten for Bogafjell')

    def set_plugin_text_strings(self):
        """
        Called to define all translatable texts of the plugin
        """
        # Name PluginList
        self.text_strings[StringContent.Name] = {
            'singular': translate('PcbPlugin', 'Pcb',
                                  'name singular'),
            'plural': translate('PcbPlugin', 'Pcb',
                                'name plural')
        }
        # Name for MediaDockManager, SettingsManager
        self.text_strings[StringContent.VisibleName] = {
            'title': translate('PcbPlugin', 'Pcb',
                               'container title')
        }
        # Middle Header Bar
        tooltips = {
            'load': '',
            'import': translate('PcbPlugin', 'Import All Plan Items '
                                'into Current Service'),
            'new': '',
            'edit': '',
            'delete': '',
            'preview': '',
            'live': '',
            'service': '',
        }
        self.set_plugin_ui_text_strings(tooltips)

    def finalise(self):
        """
        Tidy up on exit
        """
        log.info('Pcb Finalising')
        self.import_planning_center.setVisible(False)
        super(PcbPlugin, self).finalise()
